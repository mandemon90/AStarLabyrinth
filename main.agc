// Project: AStarLabyrinth 
// Created: 2016-01-06
// Author: Tommi Mansikka
// Email: tomppa.mansikka@hotmail.com

//Include files
#Include "Astar.agc"

// set window properties
SetWindowTitle( "AStarLabyrinth" )
SetWindowSize( 816, 876, 0 )

// set display properties
SetVirtualResolution( 816, 916 )
SetOrientationAllowed( 1, 1, 1, 1 )

//Structs
TYPE Tile
	sprite AS INTEGER
	access AS INTEGER	
ENDTYPE

TYPE Coordinate
	x AS INTEGER
	y AS INTEGER
ENDTYPE


TYPE Node
	position AS Coordinate
	path AS Coordinate[]
	extraPath AS Coordinate[] //This is for optimization. It is easier to save 500 entry array than 1000...
	g AS FLOAT
	h AS FLOAT
	f AS FLOAT
ENDTYPE

//Global variables
GLOBAL state 	AS INTEGER //0 = init, 1 = edit mode, 2 = run. 3 = paint path
GLOBAL startPoint 	AS Coordinate
GLOBAL endPoint 	AS Coordinate
GLOBAL exitNode 	AS Node
GLOBAL nodesProcessed AS INTEGER
GLOBAL pathLength 	AS INTEGER
GLOBAL steps		AS INTEGER

//Buttons, constants to improve performance
#CONSTANT editButton 		1
#CONSTANT runButton  		2
#CONSTANT exitButton 		3
#CONSTANT resetButton 		4
#CONSTANT startButton 		5
#CONSTANT endButton			6
#CONSTANT randomButton		7

//Constants
#CONSTANT height 50
#CONSTANT width 50
#CONSTANT offset 90
#CONSTANT stateText 1
#CONSTANT pathText 2

//Map and pathfinding
GLOBAL DIM map[height,width] AS Tile
GLOBAL lastTileEdited AS INTEGER
//Do initialization
init()

//Main loop
DO
	SELECT state
		CASE 1:
				edit()
		ENDCASE
		CASE 2:
			run()
			ENDCASE
		CASE 3:
			paint()				
			ENDCASE
		CASE 4:
			setStartPoint()
			ENDCASE
		CASE 5:
			setEndPoint()
			ENDCASE
		CASE DEFAULT:
			Log("Error has happened, state is incorrect. Ending program")
			END //If we reach this, we have an error
		ENDCASE
	ENDSELECT
	
    //Print( ScreenFPS() )    
    Sync()
    
LOOP

FUNCTION init()
	
	CreateText(stateText, "ERROR")
	setState(0)
	nodesProcessed = 0
	pathDistance = 0	
	pathFound = 0
	pathMethod = 0
	steps = 0
	lastTileEdited = 0
	LoadImage(1, "tileset.png")
	LoadImage(2, "exit.png")
	LoadImage(3, "edit.png")
	LoadImage(4, "run.png")
	LoadImage(5, "reset.png")
	LoadImage(6, "start.png")
	LoadImage(7, "end.png")
	LoadImage(8, "random.png")
	//Set up UI
	CreateSprite(exitButton, 2)
	CreateSprite(editButton,3)
	CreateSprite(runButton,4)
	CreateSprite(resetButton,5)
	CreateSprite(startButton,6)
	CreateSprite(endButton, 7)
	CreateSprite(randomButton, 8)
	
	SetSpritePosition(exitButton, 0, 5)
	SetSpritePosition(editButton, 69, 5)
	SetSpritePosition(runButton, 138, 5)
	SetSpritePosition(resetButton, 207, 5)
	SetSpritePosition(startButton, 276, 5)
	SetSpritePosition(endButton, 343, 5)
	SetSpritePosition(randomButton, 412, 5)
	
	SetTextPosition(stateText, 5, 42)
	SetTextSize(stateText, 16)
	
	CreateText(pathText, "")
	SetTextPosition(pathText, 5, 58)
	SetTextSize(pathText, 16)
	//Set up map
	
	FOR y = 0 TO height STEP 1
		FOR x = 0 TO width STEP 1
			sprite AS INTEGER
			sprite = CreateSprite(1)
			SetSpritePosition(sprite, 16*x, 16*y + offset)
			SetSpriteAnimation(sprite, 16, 16, 8)
			PlaySprite(sprite, 1, 1, 1, 1)
			map[y,x].sprite = sprite
			map[y,x].access = 1
		NEXT x
	NEXT y
	
	//Setting up top line
	FOR i = 0 TO width STEP 1
		PlaySprite(map[0,i].sprite, 1, 1, 2, 2)
		map[0,i].access = 0
	NEXT i
	
	FOR i = 0 TO width STEP 1
		PlaySprite(map[height,i].sprite, 1, 1, 2, 2)
		map[height,i].access = 0
	NEXT i
	
	
	FOR i = 0 TO height STEP 1
		PlaySprite(map[i,height].sprite, 1, 1, 2, 2)
		map[i,height].access = 0
	NEXT i
	
	FOR i = 0 TO height STEP 1
		PlaySprite(map[i,0].sprite, 1, 1, 2, 2)
		map[i,0].access = 0
	NEXT i
	
	
	startpoint.x = 1
	startPoint.y = 1
	
	endPoint.x = 49
	endPoint.y = 49
	PlaySprite(map[startpoint.x,startPoint.y].sprite, 1, 1, 5, 5)
	PlaySprite(map[endPoint.x,endPoint.y].sprite, 1, 1, 6, 6)
	
	setState(1)
	
ENDFUNCTION

//Resets the map
FUNCTION reset()
	
	FOR y = 1 TO 49 STEP 1
		FOR x = 1 TO 49 STEP 1
			PlaySprite(map[y,x].sprite, 1, 1, 1, 1)
			map[y,x].access = 1
		NEXT x
	NEXT y
	
	pathFound = 0
	SetTextString(pathText, "")
	startPoint.x = 1
	startPoint.y = 1
	endPoint.x = 49
	endPoint.y = 49
	
	//Restore start and end points
	PlaySprite(map[startPoint.y, startPoint.x].sprite, 1, 1, 5, 5)
	PlaySprite(map[endPoint.y,endPoint.x].sprite, 1, 1, 6, 6)
	
ENDFUNCTION

//Hide path without messing with blocks
FUNCTION hidePath()
	
	FOR y = 1 TO 49 STEP 1
		FOR x = 1 TO 49 STEP 1
			
			//Skip if we dealing with start or end point
			IF x = startPoint.x AND y = startPoint.y THEN CONTINUE
			IF x = endPoint.x AND y = endPoint.y THEN CONTINUE
			
			IF map[y,x].access = 1 THEN PlaySprite(map[y,x].sprite, 1, 1, 1, 1)
		NEXT x
	NEXT y
	
	pathFound = 0
	
ENDFUNCTION

FUNCTION clearArrays()
	//Clear  sets
	WHILE openSet.length > -1
		openSet.remove()
	ENDWHILE		
	WHILE closedSet.length > -1
		closedSet.remove()
	ENDWHILE
	

ENDFUNCTION

//Main edit mode cose
FUNCTION edit()
		
	IF GetPointerReleased() = 1
		sprite AS INTEGER
		sprite = GetSpriteHit(GetPointerX(), GetPointerY() ) //Better store value than keep calling the function
		
		SELECT sprite
			CASE exitButton:
				END
				ENDCASE
			CASE editButton:
				setState(1)
				ENDCASE
			CASE runButton:
				startRun()
				ENDCASE
			CASE resetButton
				reset()
				ENDCASE
			CASE startButton:
				setState(4)
				ENDCASE
			CASE endButton:
				setState(5)
				ENDCASE
			CASE randomButton:
				randomMap()
				ENDCASE
			CASE DEFAULT:
				editTile(sprite)
				ENDCASE
		ENDSELECT
		
	ELSEIF GetPointerState() = 1
		editTile( GetSpriteHit(GetPointerX(), GetPointerY() ) )
	ELSE
		lastTileEdited = 0
	ENDIF
	
ENDFUNCTION

//Prepare to start running when the next cycle begins
FUNCTION startRun()
	nodesProcessed = 0
	pathDistance = 0
	steps = 0
	clearArrays()
	startNode AS Node
	startNode.position.x = startPoint.x
	startNode.position.y = startPoint.y
	startNode.g = 0
	startNode.h = Sqrt( Pow( (endPoint.x - startNode.position.x), 2 ) + Pow( (endPoint.y - startNode.position.y), 2 ) )
	startNode.f = startNode.g + startNode.h
	WHILE startNode.path.length >= 0
		startNode.path.remove()
	ENDWHILE
	
	addToOpenSet(startNode)
	
	setState(2)
	SetTextString(pathText, "")
ENDFUNCTION

//Using this to open room for later expansion
FUNCTION run()
		
	pathFound AS INTEGER
	pathFound = aStar()
	steps = steps + 1
	
	// if path has been found, paint it
	IF pathFound = 1
		setState(3)
	ENDIF
	
ENDFUNCTION

//Paints the route taken
FUNCTION paint()
	
	
	//Paint until we reach the last position and 
	IF exitNode.path.length >= 0
		p AS Coordinate
		p = exitNode.path[0]
		PlaySprite(map[p.y, p.x].sprite, 1, 1, 4, 4)
		exitNode.path.remove(0)
	ELSE
		PlaySprite(map[startPoint.y, startPoint.x].sprite, 1, 1, 5, 5)
		pathFound = 0
		setState(1)		
	ENDIF
	
	Sleep(20)
	
ENDFUNCTION

FUNCTION setStartPoint()
	
	IF GetPointerReleased() = 1
		sprite AS INTEGER
		sprite = GetSpriteHit(GetPointerX(), GetPointerY() ) //Better store value than keep calling the function
				
		SELECT sprite
			CASE exitButton:
				END
				ENDCASE
			CASE editButton:
				setState(1)
				ENDCASE
			CASE DEFAULT:
				c as Coordinate
				c = getTile(sprite)
				
				//Set graphics of old one
				oldSprite AS INTEGER
				oldSprite = map[startPoint.y, startPoint.x].sprite
				PlaySprite(oldSprite, 1, 1, 1, 1)
				//Set up new target
				startPoint.x = c.x
				startPoint.y = c.y
				
				map[c.y, c.x].access = 1
				PlaySprite(map[c.y, c.x].sprite, 1, 1, 5, 5)
				SetState(1)
				ENDCASE
		ENDSELECT
		
	ENDIF
ENDFUNCTION

FUNCTION setEndPoint()
	IF GetPointerReleased() = 1
		sprite AS INTEGER
		sprite = GetSpriteHit(GetPointerX(), GetPointerY() ) //Better store value than keep calling the function
				
		SELECT sprite
			CASE exitButton:
				END
				ENDCASE
			CASE editButton:
				setState(1)
				ENDCASE
			CASE DEFAULT:
				c as Coordinate
				c = getTile(sprite)
				
				//Set graphics of old one
				oldSprite AS INTEGER
				oldSprite = map[endPoint.y, endPoint.x].sprite
				PlaySprite(oldSprite, 1, 1, 1, 1)
				//Set up new target
				endPoint.x = c.x
				endPoint.y = c.y
				
				map[c.y, c.x].access = 1
				PlaySprite(map[c.y, c.x].sprite, 1, 1, 6, 6)
				SetState(1)
				ENDCASE
		ENDSELECT
		
	ENDIF
ENDFUNCTION

FUNCTION editTile(sprite AS INTEGER)
	
	hidePath()
	
	FOR y = 1 TO 49 STEP 1
		FOR x = 1 TO 49 STEP 1
			
			IF map[y,x].sprite = sprite
				
				//Check that we are messing with start or end, if so, skip checking
				IF (x = 1 AND y = 1) OR (x = 49 AND y = 49) THEN CONTINUE
				
				//Incase the tile was the last one to be edited, don't mess with it. This is to allow drag-and-paint
				IF lastTileEdited = sprite 
					EXIT
				ENDIF
				
				//If tile is accessible, set to non accessible. And reverse
				IF map[y,x].access = 1 
					map[y,x].access = 0
					PlaySprite(map[y,x].sprite, 1, 1, 2, 2)
				ELSE
					map[y,x].access = 1
					PlaySprite(map[y,x].sprite, 1, 1, 1, 1)
				ENDIF 			
					
				lastTileEdited = map[y,x].sprite				
			ENDIF
		NEXT x
	NEXT y
	
ENDFUNCTION

FUNCTION setState(newState AS INTEGER)
	
	SELECT newState
		CASE 0:
			SetTextString(stateText,"Initializing")
		ENDCASE
		CASE 1:
			SetTextString(stateText,"Edit mode")
		ENDCASE
		CASE 2:
			SetTextString(stateText,"Finding path ")
		ENDCASE
		CASE 3:
			pathLength = exitNode.path.length
			SetTextString(stateText,"Painting" )			
			SetTextString(pathText,"Path found, length " + Str(pathLength) + ", " + Str(nodesProcessed) + " nodes were processed, out of total " + Str(openSet.length + closedSet.length) )
		ENDCASE
		CASE 4:
			SetTextString(stateText,"Please click starting point")
		ENDCASE
		CASE 5:
			SetTextString(stateText,"Please click end point")
		ENDCASE
		CASE DEFAULT
			SetTextString(stateText,"ERROR")
		ENDCASE
	ENDSELECT
	
	state = newState
	
ENDFUNCTION

FUNCTION getTile(sprite AS INTEGER)
	c AS Coordinate
	c.x = 1
	c.y = 1
	FOR y = 1 TO 49 STEP 1
		FOR x = 1 TO 49 STEP 1			
			IF map[y,x].sprite = sprite 
				c.x = x
				c.y = y
				EXITFUNCTION c
			ENDIF
		NEXT x
	NEXT y
	
ENDFUNCTION c


FUNCTION checkIfTarget(test AS Node)
	
	cord AS Coordinate
	cord = test.position
	success = 0
	
	IF cord.x = endPoint.x AND cord.y = endPoint.y THEN success = 1

ENDFUNCTION success

FUNCTION randomMap()
	//Make sure we don't mess with the border
	FOR y = 1 TO height-1 STEP 1
		FOR x = 1 TO width-1 STEP 1
			
			//If we are messing with start/end point, skip
			IF y = startPoint.y AND x = startPoint.x THEN CONTINUE
			IF y = endPoint.y AND x = endPoint.x THEN CONTINUE
			
			//If we are not dealing with end or start points (which must always remain 1), roll for access
			map[y,x].access = Random(0, 1)
			IF map[y,x].access = 1 
				map[y,x].access = 0
				PlaySprite(map[y,x].sprite, 1, 1, 2, 2)
			ELSE
				map[y,x].access = 1
				PlaySprite(map[y,x].sprite, 1, 1, 1, 1)
			ENDIF 
		NEXT x
	NEXT y
ENDFUNCTION
