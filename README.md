# A* LABYRINTH
---

This is a small program made on spare time. Language is BASIC with AGK functionality for the graphics. Source code for all versions are provided. There will be 20ms delay between taking 
steps to help provide visualization.

Has performance drop sharply after roughly 500 nodes, due to increasing amount of arrays it needs to shift array. If I were to implement this in C++, rather than in Tier 1 BASIC, I would use pointers to node themselves, rather than arrays of coordinates. As for BASIC, as far as I have understood, this is not possible, so arrays it is.

## USE
---
Hold and drag to change tiles. Press RUN to find path. Press START / END to set the start and end points, EDIT to cancel. Press RESET to remove the current map. Press RANDOM to generate random map. END to exit the program.

## COLOR CODES
---
* Light Grey: Open square
* Dark Gret: Closed square
* Light Red: Tile in open set (To be checked)
* Dark Red: Tile in closed set (Checked)
* Light Green: Tile being checked
* Blue: Start point
* Purple: End point

## VERSION HISTORY
---

### 1.3
---
* Random map generation

### 1.2
---
* Cleanup of the code.
* Additional info now displayed, such as how many nodes were processed, how many steps were taken and how long the final path is
* Separated algorith to different file for clarity

### 1.1
---
* Improved algorithm. Instead of checking the closest tile to the end, it uses F-score
* Nodes now have g, h and f scores.
* Ability to set start and end points.


### 1.0
---
Initial release. Basic program that looks for route from top-left point to bottom-right corner. User can block tiles to force the program to seek a new path.
* Can look for paths
* Will report if it can no longer proceed and has not found path
* If a path is found, paints the route it took 