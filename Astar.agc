GLOBAL openSet AS Node[]
GLOBAL closedSet AS Node[]

//A* algorith
FUNCTION aStar()
	
	now AS Node
	nextNode As Node
	
	index AS INTEGER
	index = findLowest()
	
	//If we have exhaisted the list
	IF index = -1 
		setState(1)
		SetTextString(pathText,"No path found")
		EXITFUNCTION 0
	ENDIF
	
	now = openSet[index]
	sprite AS INTEGER
	sprite = map[now.position.y, now.position.x].sprite
	
	PlaySprite(sprite, 1, 1, 7, 7)
	Sync()
	//Sleep(10)
	nextNode.path = now.path
	nextNode.path.insert(now.position)
	
	//Start from top-left and then check others in clock wise direction	
	FOR y = 1 TO -1 STEP -1
		
		FOR x = -1 TO 1 STEP 1
			
			IF y = 0 AND x = 0 THEN CONTINUE //skip the current tile
			
			//If can move to tile, set that as the next node and safe
			IF map[now.position.y + y, now.position.x + x].access = 1
				
				
				
				nextNode.position.x = now.position.x + x
				nextNode.position.y = now.position.y + y
				nextNode.g = now.g + Sqrt( Pow(y, 2) + Pow(x, 2))
				nextNode.h = Sqrt( Pow( (endPoint.x - nextNode.position.x), 2 ) + Pow( (endPoint.y - nextNode.position.y), 2 ) )
				nextNode.f = nextNode.g + nextNode.h
				
				//See if the node is target node
				IF checkIfTarget(nextNode) = 1
					exitNode = nextNode
					//Remove the first node from the list, since it's the starting node
					//exitNode.path.remove(0)
					setState(3)
					EXITFUNCTION 1
				ENDIF
				//See if already checked. If not, add it to be checked
				IF aStarSeeIfVisited(nextNode) = 0 
					openSet.insert(nextNode)
					p AS Coordinate
					p = nextNode.position
					PlaySprite(map[p.y, p.x].sprite, 1, 1, 8, 8)		
				ENDIF
			ENDIF
			
		NEXT x
	NEXT Y
	//We have exhausted current node, check it and change color	
	PlaySprite(sprite, 1, 1, 3, 3)
	closedSet.insert(now)
	openSet.remove(index)
	nodesProcessed = nodesProcessed + 1
	SetTextString(pathText, Str(closedSet.length) + " nodes processed, " + Str(openSet.length) + " in openset")
ENDFUNCTION 0
	
FUNCTION findLowest()
	
	//If we have run out of nodes to check, return -1
	IF(openSet.length < 0) THEN EXITFUNCTION -1
	
	closest AS Node
	now AS Node
	
	index AS INTEGER
	index = 0
	//Set up first one as the closest one
	closest = openSet[index]
	//See which one is closest to end point
	FOR i = 0 TO openSet.length STEP 1
		now = openSet[i]
		//distance = Sqrt( Pow( (endPoint.x - now.position.x), 2) + Pow( (endPoint.y - now.position.y), 2) )
		
		//If the dostance from currently being checked is shorten than the distance from the closest
		IF now.f < closest.f
			//shortest = distance
			closest = now
			index = i
		ENDIF
		
	NEXT i
	
ENDFUNCTION index


// See if node has been checked, if yes, return 1, otherwise, return 0
FUNCTION aStarSeeIfVisited(test AS Node)
	
	isChecked AS INTEGER
	isChecked = 0
	now AS Node
	//First see if it has already been discoutned
	FOR i = 0 TO closedSet.length STEP 1
		now = closedSet[i]		
		IF(now.position.x = test.position.x) AND (now.position.y = test.position.y) //THEN EXITFUNCTION 1
			IF now.g > test.g THEN closedSet[i] = test
			EXITFUNCTION 1				
		ENDIF
	NEXT i
	//If not, see if it is on to-do list
	FOR i = 0 TO openSet.length STEP 1
		now = openSet[i]		
		IF(now.position.x = test.position.x) AND (now.position.y = test.position.y) //THEN EXITFUNCTION 1
			IF now.g > test.g THEN openSet[i] = test
			EXITFUNCTION 1				
		ENDIF	
	NEXT i
	
ENDFUNCTION 0

//This is purely for good coding practice, keep stuff local to the file
FUNCTION addToOpenSet(entry AS Node)
	openSet.insert(entry)
ENDFUNCTION
